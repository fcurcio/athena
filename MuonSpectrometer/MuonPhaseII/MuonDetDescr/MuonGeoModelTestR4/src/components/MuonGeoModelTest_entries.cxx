
/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#include "../GeoModelMdtTest.h"
#include "../GeoModelRpcTest.h"
#include "../GeoModelTgcTest.h"
#include "../GeoModelMmTest.h"
#include "../MuonChamberToolTest.h"
#include "../GeoModelsTgcTest.h"

DECLARE_COMPONENT(MuonGMR4::GeoModelMdtTest)
DECLARE_COMPONENT(MuonGMR4::GeoModelRpcTest)
DECLARE_COMPONENT(MuonGMR4::GeoModelTgcTest)
DECLARE_COMPONENT(MuonGMR4::GeoModelsTgcTest)
DECLARE_COMPONENT(MuonGMR4::GeoModelMmTest)
DECLARE_COMPONENT(MuonGMR4::MuonChamberToolTest)
